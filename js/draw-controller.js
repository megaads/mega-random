var app = angular.module('myApp', []);
app.directive('focusMe', ['$timeout', '$parse', function ($timeout, $parse) {
    return {
        link: function (scope, element, attrs) {
            var model = $parse(attrs.focusMe);
            scope.$watch(model, function (value) {
                if (value === true) {
                    $timeout(function () {
                        element[0].focus();
                    });
                }
            });
            element.bind('blur', function () {
                scope.$apply(model.assign(scope, false));
            });
        }
    };
}]);
app.controller('drawController', function ($scope) {
    $scope.state = -1;
    $scope.inprocess = false;
    $scope.numberOfTickets = "";
    $scope.resultSet = [];
    $scope.randomNumber = "$$$";

    $scope.enter = function (keyEvent) {
        if (keyEvent.which === 13) {
            if ($scope.state == -1) {
                $scope.state = 0;
            }
            // enter ticket number
            else if ($scope.state == 0) {
                $scope.state = 1;
            }
            // draw
            else if ($scope.state == 1 && $scope.inprocess == false) {
                draw();
            }
        }
    }

    function draw() {
        $scope.inprocess = true;
        suffle(function () {
            var number = random(true);
            $scope.$apply(function () {
                $scope.randomNumber = number;
                $scope.resultSet.push(number);
            });
            $scope.inprocess = false;
        });
    }

    function suffle(callbackFn) {
        var interval = setInterval(function () {
            $scope.$apply(function () {
                $scope.randomNumber = random();
            });
        }, 50);
        setTimeout(function () {
            clearInterval(interval);
            callbackFn();
        }, 5000);
    }

    function random(excludeResultSet) {
        var retval = Math.floor(Math.random() * ($scope.numberOfTickets - 1 + 1)) + 1;
        if (excludeResultSet == true) {
            var excluded = false;
            for (var i = 0; i < $scope.resultSet.length; i++) {
                if (retval == $scope.resultSet[i]) {
                    excluded = true;
                    break;
                }
            }
            if (excluded) {
                retval = random(true);
            }
        }
        return retval;
    }
});
